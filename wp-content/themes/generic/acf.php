<?php
/*
Template Name: Добавление мероприятия
*/

acf_form_head();
get_header();




$args = array(
	'post_id' => 'new_post',
	'new_post' => array(
		'post_type' => 'event',
		'post_status' => 'draft',
	),
	'post_title' => true,
	'submit_value' => 'Создать',
	'updated_message' => 'Ваша запись поставлена в очередь на модерацию',
	'label_placement' => 'left',
);
?>

<div class="pattern-container">
	<div class="site-container">
        <h2>Добавить мероприятие:</h2>
		<?php acf_form( $args ); ?>
	</div>
</div>


