<?php
get_header();?>

<div class="pattern-container">
    <div class="site-container archive-events-container">
		<?php
		if(have_posts()) : while(have_posts()) : the_post();
			get_template_part( 'entry-event' );

		endwhile; endif;?>
    </div>
</div>


<?

get_footer();
?>