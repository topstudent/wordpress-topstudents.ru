<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>"/>
    <meta name="viewport" content="width=device-width"/>
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="header-frame">


    <div id="header-container">
        <div id="header-logo">
        </div>
        <div class="header-menu">
            <ul class="header-ul">
                <li class="header_li"><a href="">Главная</a></li>
                <li class="header_li"><a href="event">Мероприятия</a></li>
                <li class="header_li"><a href="">Скидки</a></li>
            </ul>
        </div>
        <div class="login-btn" id="header-buttons">
            <button style="width: 100%" class="auth_btn primary_btn">Войти</button>
            <button class="test secondary_btn">Регистрация</button>

        </div>
    </div>

</div>
    <div id="container">