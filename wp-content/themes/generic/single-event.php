<?php get_header(); ?>

<div class="pattern-container">

    <div class="site-container">

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <h2><?php the_title(); ?></h2>

            <div class="content-info">
				<?php $image = get_field( 'gallery' )[0]; ?>
                <img class="content-image" src="<?php echo $image['sizes']['large']; ?>"/>
                <div class="content-text-info">
                    <div class="content-meta-info-item">
                        <div class="icon"><i class="fa fa-user-o" aria-hidden="true"></i></div>
                        <h5>Организатор: </h5> <?php the_author() ?>
						<?php $geo_info = get_field( 'geo_info' ); ?>
						<?php if ( $geo_info ) { ?>
                            <div class="icon"><i class="fa fa-map-marker" aria-hidden="true"></i></div> <h5>Адрес: </h5> <?php echo $geo_info['address']; ?>
						<?php } ?>

                        <div class="icon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                        <h5>Начало: </h5> <?php the_field( 'date' ); ?>

                    </div>

					<?php if ( get_field( 'reg_type' ) != "none" ) { ?>
                        <button onclick="" class="primary_btn">Регистрация</button>
					<?php } ?>
                    <div class="content-description-info">
                        <p><?php echo the_field( 'description' ); ?></p>
                    </div>

                </div>

            </div>

            <div class="gallery">
				<?php $gallery_images = get_field( 'gallery' ); ?>
				<?php if ( $gallery_images ) : ?>
					<?php foreach ( $gallery_images as $gallery_image ): ?>
                        <a href="<?php echo $gallery_image['url']; ?>">
                            <img src="<?php echo $gallery_image['sizes']['thumbnail']; ?>"
                                 alt="<?php echo $gallery_image['alt']; ?>"/>
                        </a>
                        <p><?php echo $gallery_image['caption']; ?></p>
					<?php endforeach; ?>
				<?php endif; ?>
            </div>


			<?php
			if ( get_field( 'reg_type' ) === "portal" ) { ?>
                <form method="post"  id="event-reg" action="#" class="event-registration-form">
                    <input name="first_name" type="text" placeholder="Имя">
                    <input name="last_name" type="text" placeholder="Фамилия">
                    <input name="patronymic" type="text" placeholder="Отчество">
                    <input name="phone" type="phone" placeholder="Телефон">
                    <input name="email" type="email" placeholder="Почта">
                    <input name="university" type="text" placeholder="ВУЗ">
                    <button style="margin: auto; width: 200px; grid-column: span 2;" type="submit" name="reg_user" class="primary_btn">
                        Принять участие
                    </button>
                </form>
			<?php }
			?>


			<?php

        if(isset($_POST['reg_user'])){

            $first_name = $_POST['first_name'];
            $last_name = $_POST['last_name'];
            $patronymic = $_POST['patronymic'];
            $phone = $_POST['phone'];
            $email_user = $_POST['email'];
            $university = $_POST['university'];

            global $wpdb;

            $wpdb->query($wpdb->prepare(
                    "INSERT INTO `event_registrations` (`id_registration`, `id_user`, `id_event`, `date_registration`, `first_name`, `last_name`, `email`, `phone`, `patronymic`, `university`) VALUES (NULL, %d,%d,current_timestamp(),%s,%s,%s,%s,%s,%s)",
                    array(get_current_user_id(), get_the_ID(), $first_name, $last_name, $email_user, $phone, $patronymic, $university)
            ));


        }

			?>


			<?php if ( comments_open() && ! post_password_required() ) {
				comments_template( '', true );
			} ?>
		<?php endwhile; endif; ?>

    </div>


</div>

