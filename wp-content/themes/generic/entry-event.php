<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="entry-event-container">
        <a href="<?php the_permalink(); ?>">

            <div class="entry-event-date">
			    <?php echo the_field( 'date' ); ?>
            </div>

		    <?php $image = get_field( 'gallery' )[0]; ?>
            <div class="entry-event-img" style="background-image: url(<?php echo $image['sizes']['medium']; ?>)"></div>


            <div class="entry-event-title-box">
                <a class="entry-event-title" href="<?php the_permalink(); ?>"
                   title="<?php the_title_attribute(); ?>"
                   rel="bookmark"><?php the_title(); ?></a>
            </div>

        </a>
    </div>


		<?php if ( is_singular() ) {
		get_template_part( 'entry-footer' );
	} ?>
</article>