��          �   %   �      `     a  
   u     �  /   �     �  
   �     �     �        1     W   H  =   �     �  
   �     �  !   �       F   %     l     |     �     �  \   �            U   %  S  {  -   �     �  $     ;   5  
   q     |  #   �  @   �  %   �  B   	  �   b	  V   �	     =
  !   I
  $   k
  9   �
     �
  w   �
     T  %   j     �  (   �  �   �  
   x  )   �  o   �                      	                                    
                                                                  &larr; Back to home Categories Comments are closed. Edit <span class="screen-reader-text">%s</span> Header Install %s Install Plugins Install Required Plugins Installing Plugin: %s It looks like nothing was found at this location. It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Leave a Comment<span class="screen-reader-text"> on %s</span> Menu Not found. Nothing Found One thought on &ldquo;%1$s&rdquo; Pages: Ready to publish your first post? <a href="%1$s">Get started here</a>. Search &hellip; Search Results for: %s Search for: Skip to content Sorry, but nothing matched your search terms. Please try again with some different keywords. Tags Updating Plugin: %s comments title%1$s thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; Project-Id-Version: Justread v10.0.9
Report-Msgid-Bugs-To: 
POT-Creation-Date: 
PO-Revision-Date: 2019-04-16 12:14+0300
Last-Translator: Игорь <dybabiz@ya.ru>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.8.12
X-Poedit-SourceCharset: utf-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
Language: ru
X-Poedit-SearchPath-0: .
 &larr; Вернуться на главную Категории Обсуждение закрыто. Изменить <span class="screen-reader-text">%s</span> Шапка Установить %s Установка плагинов Установить дополнительные плагины Установка плагина: %s По данному адресу ничего не найдено. Кажется, мы не можем найти то, что вы ищете. Возможно, поиск может помочь. Оставить комментарий<span class="screen-reader-text">на %s</span> ﻿Меню Ничего не найдено. Страница не найдена Один комментарий для &ldquo;%1$s&rdquo; Страницы: Готовы опубликовать свою первую запись? <a href="%1$s">Начните здесь</a>. Искать &hellip; Результаты поиска: %s Поиск для: Перейти к содержимому К сожалению, но по вашему запросу совпадений не найдено. Попробуйте еще раз с другими словами. Метки Обновление плагинов: %s %1$s комментариев для &ldquo;%2$s&rdquo; %1$s комментариев для &ldquo;%2$s&rdquo; 