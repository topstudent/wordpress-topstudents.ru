﻿<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'wordpress_db' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'root' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', '' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'm?H7]C<BYp1qpdal%c,xf.bdC@bL$0kK)l r&;&5}TbZzMJKV-Nr40q9,0So#ZlT' );
define( 'SECURE_AUTH_KEY',  'q&@HZ_vHJ_5D(n^Gq,R(q:ufbHv9gOr,mn?~|p,6KM)C/#VcnK`5p52t6i2n_EOK' );
define( 'LOGGED_IN_KEY',    '%o-j,UUZ@:lCd#^q|h0mOtP%*;EF<vot9xRy9O{TJ= i:(&xI7u*%K$4FX<@+SHV' );
define( 'NONCE_KEY',        ';SQ}BeL1hCVmO&nIE3[vJBz:p=T%shW{!4b#?$1b~ZPWA8/,Hp++E6?[~AJP42D4' );
define( 'AUTH_SALT',        'HiozrCO0-$a-@Zsrv3{d38uA[Pi6 @{K`7 4s$eU{HB~{TuZd+h {N#*= Q%&LSv' );
define( 'SECURE_AUTH_SALT', '>?K:jj<a>BvR9@-Bk#,`Gd 2}KINd2}jA;@|zy?XZjVC+wS`@;4C 7,GyY/EPrnm' );
define( 'LOGGED_IN_SALT',   'y)#U}w^4:;=d<iWOJ}A3xkOpU{F>Tyw}8dTchH;oP<stF2m!#-c2O udB}gIDt/.' );
define( 'NONCE_SALT',       ':,k^cH:[K fsTvI[!4&.Sj1wOgcef:vUhy3.KSk*kH@+_(]uphB_qEeQP &9)fli' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
